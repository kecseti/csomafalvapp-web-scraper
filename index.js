const got = require('got');
const jsdom = require("jsdom");
const { JSDOM } = jsdom;
const fetch = require("node-fetch");

const newsScraper =async () => {
  console.log("Starting news scraper");
  // Scrape down the information
  const response = await got('http://www.csomafalva.info/kozelet/hirek');
  const dom = new JSDOM(response.body);
  const nodeList = dom.window.document.getElementsByClassName('views-row');

  Array.from(nodeList).forEach( async (node) => {
    let type = 1;
    // Get what we need
    const title = node.getElementsByClassName('views-field-title')[0].getElementsByClassName('field-content')[0].childNodes[0].innerHTML;
    if(title.includes('Gyászjelentő')){ type = 3;}

    const description = node.getElementsByClassName('views-field-field-hirek-felvezeto-value')[0].getElementsByClassName('field-content')[0].innerHTML.replace(/&nbsp;/g, ' '); 
    if(description.toLowerCase().includes('felhívjuk')){ type = 2; }
    const imageURL = node.getElementsByClassName('views-field-field-hirek-kep-fid')[0].getElementsByClassName('field-content')[0].childNodes[0].childNodes[0].getAttribute("src"); 

    const url = node.getElementsByClassName('views-field-nothing')[0].getElementsByClassName('field-content')[0].childNodes[0].getAttribute("href").replace('/..','http://www.csomafalva.info/hirek');

    // Check if it already exists on server
    const rawAnswer = await fetch("https://csomafalapi.vercel.app/api/news/0/5000/15");
    const response = await rawAnswer.json();
    
    let found = false;
    response.news.forEach(n => {
      if(n.title == title && n.description == description){
        found = true;
      }
    })

    // Upload using the API
    if(found===false){
      const myHeaders = new fetch.Headers();
      myHeaders.append("Content-Type", "application/json");
      const raw = JSON.stringify({title:title, description:description,type:type,photoURL:imageURL, url: url});

      const requestOptions = {method: 'POST', headers: myHeaders, body: raw, };
      fetch("https://csomafalapi.vercel.app/api/news", requestOptions)
    }
  })
  console.log("News scraper finished");
};

const churchScraper = async() => {
  console.log("Starting church scraper");
  const res = await got("http://www.csomafalva.info/egyhaz");
  const dom = new JSDOM(res.body);
  const nodeList = dom.window.document.querySelectorAll('[style="text-align: center;"]');
  let miserendURL = 'not found';
  for(let i = 0; i < nodeList.length; i++){ 
    if(nodeList[i].childNodes[0].nodeName.toLowerCase() === 'img'){
      miserendURL = `http://www.csomafalva.info/${nodeList[i].childNodes[0].getAttribute("src")}`;
      break;
    }
  }
  console.log(miserendURL);
  /*
  // Check if it already exists on server
  const rawAnswer = await fetch("https://csomafalapi.vercel.app/api/news/0/5000/15");
  const response = await rawAnswer.json();

  let found = false;
  response.news.forEach(n => {
    if(n.title == miserendURL){
      found = true;
    }
  })

  // Upload using the API
  if(found===false){
    const myHeaders = new fetch.Headers();
    myHeaders.append("Content-Type", "application/json");
    const raw = JSON.stringify({title:'Miserend', description:'miserend', type:4,photoURL:miserendURL});

    const requestOptions = {method: 'POST', headers: myHeaders, body: raw, };
    fetch("https://csomafalapi.vercel.app/api/news", requestOptions)
  }
  */
  console.log('Church scraper finished');
}

scraper = () => {
  newsScraper();
  churchScraper();
}

scraper();
